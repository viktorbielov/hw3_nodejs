# Cargo Uber

Hello! This app is UBER like service for freight trucks. It is built in REST style, using MongoDB and node.js.

## Quick-start

1. run "npm i"
2. run "npm start"

Server starts on port 8080 by default. App is running on addres: http://localhost:8080/
Use api on http://localhost:8080/api
Enjoy!