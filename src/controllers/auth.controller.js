const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const passwordCreator = require('generate-password');
const { mailer } = require('../utils/mailClient');
const { User } = require('../models/User');
// eslint-disable-next-line
const register = async (req, res, next) => {
  try {
    const { role, email, password } = req.body;
    const user = new User({
      role,
      email,
      password: await bcrypt.hash(password, 10),
    });
    await user.save();
    res.status(200).json({ message: 'Profile created successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};
// eslint-disable-next-line
const login = async (req, res, next) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (await bcrypt.compare(password, user.password)) {
      const payload = {
        // eslint-disable-next-line
        userId: user._id,
        email,
        role: user.role,
      };
      const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);
      return res.status(200).json({ jwt_token: jwtToken });
    }
    return res.status(403).json({ message: 'Not authorized' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};
// eslint-disable-next-line
const forgotPassword = async (req, res, next) => {
  try {
    const { email } = req.body;
    const password = passwordCreator.generate({
      length: 10,
      numbers: true,
    });
    await User.findOneAndUpdate(
      { email },
      { $set: { password: await bcrypt.hash(password, 10) } },
    );
    mailer({
      to: email,
      subject: 'New password',
      text: `Hey! Your new password is: ${password}`,
    });
    res.status(200).json({ message: 'New password sent to your email address' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

module.exports = { register, login, forgotPassword };
