/* eslint-disable */
const { Load } = require('../models/Load');
const { Truck } = require('../models/Truck');

const retreiveActiveLoad = async (driverId) => {
  const load = await Load.findOne({
    assigned_to: driverId,
    status: 'ASSIGNED',
  });
  if (!load) {
    throw new Error('No active load at current moment');
  }
  return load;
};

const changeStatus = async (userId, truck) => {
  if (!truck) {
    truck = await Truck.findOne({
      assigned_to: userId,
    });
  }
  switch (truck.status) {
    case 'IS':
      truck.status = 'OL';
      break;
    case 'OL':
      truck.status = 'IS';
      break;
  }
  await truck.save();
};

const statuses = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery'];

const iterState = async (driverId) => {
  const load = await retreiveActiveLoad(driverId);
  if (load.state === 'Arrived to delivery') {
    throw new Error('Cannot edit finished deliveries');
  }
  load.state = statuses[statuses.findIndex((element) => element === load.state) + 1];
  if (load.state === 'Arrived to delivery') {
    load.status = 'SHIPPED';
  }
  load.logs.push({
    message: `Load state changed to '${load.state}'`,
    time: new Date(),
  });
  await load.save();
  return load.state;
};

const getShipmentLoads = async (
  userId,
  status = {
    $in: [
      'NEW',
      'POSTED',
      'ASSIGNED',
      'SHIPPED',
    ],
  },
  limit,
  offset,
) => (await Load.find({ assigned_by: userId, status })
  .skip(offset)
  .limit(limit));

const getShipperLoadInfo = async (loadId, userId) => (await Load.findOne({
  _id: loadId,
  created_by: userId,
}));

const getDriverLoads = async (
  userId,
  status = {
    $in: [
      'NEW',
      'POSTED',
      'ASSIGNED',
      'SHIPPED',
    ],
  },
  limit,
  offset,
) => (await Load.find({ assigned_to: userId, status })
  .skip(offset)
  .limit(limit));

const getDriverLoadDetails = async (loadId, userId) => (await Load.findOne({
  _id: loadId,
  assigned_to: userId,
}));

const createLoad = async (created_by, name, payload, pickup_address, delivery_address, dimensions) => {
  const load = new Load({
    created_by,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });
  await load.save();
};

const registerLoad = async (req, res, next) => {
  try {
    const {
      name, payload, pickup_address, delivery_address, dimensions,
    } = req.body;
    const { userId } = req.user;
    await createLoad(userId, name, payload, pickup_address, delivery_address, dimensions);
    res.status(200).json({ message: 'Load created successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const getUsersLoads = async (req, res, next) => {
  try {
    const { status, limit, offset } = req.params;
    const { userId, role } = req.user;
    const loads = role === 'SHIPPER' ? await getShipmentLoads(userId, status, limit, offset)
      : await getDriverLoads(userId, status, limit, offset);
    res.status(200).json({ loads });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const postUsersLoads = async (req, res, next) => {
  try {
    const { id } = req.params;
    const load = await Load.findOne({
      _id: id,
    });
    const { payload, dimensions } = load;
    if (load.assigned_to) {
      throw new Error('Cannot post already assigned load');
    }
    const truck = await Truck.findOne({
      assigned_to: { $ne: null },
      status: 'IS',
      payload: { $gt: payload },
      'dimensions.length': { $gt: dimensions.length },
      'dimensions.width': { $gt: dimensions.width },
      'dimensions.height': { $gt: dimensions.height },
    });
    if (truck) {
      load.status = 'ASSIGNED';
      load.assigned_to = truck.assigned_to;
      load.state = 'En route to Pick Up';
      load.logs = [{
        message: `Load assigned to driver with id ${truck.assigned_to}`,
        time: new Date(),
      }];
      await load.save();
      if (!truck) {
        truck = await Truck.findOne({
          assigned_to: truck.assigned_to,
        });
      }
      switch (truck.status) {
        case 'IS':
          truck.status = 'OL';
          break;
        case 'OL':
          truck.status = 'IS';
          break;
      }
      await truck.save();
      return res.status(200).json({
        message: 'Load posted successfully',
        driver_found: true,
      });
    }
    return res.status(404).json({
      message: 'At this moment there is no driver for your request',
    });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const getActiveLoad = async (req, res, next) => {
  try {
    const { userId } = req.user;
    const load = await retreiveActiveLoad(userId);
    res.status(200).json({ load });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const getLoad = async (req, res, next) => {
  try {
    const { userId, role } = req.user;
    const { id } = req.params;
    const load = role === 'SHIPPER' ? await getShipperLoadInfo(id, userId)
      : await getDriverLoadDetails(id, userId);
    res.status(200).json(load);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const toggleLoadState = async (req, res, next) => {
  try {
    const { userId } = req.user;
    const state = await iterState(userId);
    if (state === 'Arrived to delivery') {
      await changeStatus(userId);
    }
    res.status(200).json({ message: `Load state changed to '${state}'` });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const editLoadDetails = async (req, res, next) => {
  try {
    const { id } = req.params;
    const {
      name, payload, pickup_address, dilevery_address, dimensions,
    } = req.body;
    await Load.findOneAndUpdate(
      { _id: id, status: 'NEW' },
      {
        name, payload, pickup_address, dilevery_address, dimensions,
      },
    );
    res.status(200).json({ message: 'Load details changed successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const deleteLoad = async (req, res, next) => {
  try {
    const { id } = req.params;
    await Load.findOneAndDelete({
      _id: id,
      status: 'NEW',
    });
    res.status(200).json({ message: 'Load deleted successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const shippingInfo = async (req, res, next) => {
  try {
    const { id } = req.params;
    const load = await Load.findOne({ _id: id });
    let truck;
    if (load.assigned_to) {
      truck = await Truck.findOne({ assigned_to: load.assigned_to });
    }
    res.status(200).json({ load, truck });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

module.exports = {
  registerLoad,
  postUsersLoads,
  getUsersLoads,
  getLoad,
  getActiveLoad,
  editLoadDetails,
  toggleLoadState,
  shippingInfo,
  deleteLoad,
};
