const { Truck } = require('../models/Truck');
const { getDimensionsFromType, getPayloadFromType } = require('../utils/truckTypes');
// eslint-disable-next-line
const getTruck = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { userId } = req.user;
    const truck = await Truck.find({
      created_by: userId,
      _id: id,
    });
    res.status(200).json({ truck });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};
// eslint-disable-next-line
const getTrucks = async (req, res, next) => {
  try {
    const { userId } = req.user;
    const trucks = await Truck.find({ created_by: userId });
    res.status(200).json({ trucks });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};
// eslint-disable-next-line
const createTruck = async (req, res, next) => {
  try {
    const { type } = req.body;
    const { userId } = req.user;
    const status = 'IS';
    await Truck.findOneAndUpdate({
      assigned_to: userId,
    }, {
      $set: {
        assigned_to: null,
      },
    });
    const payload = getPayloadFromType(type);
    const dimensions = getDimensionsFromType(type);
    const truck = new Truck({
      created_by: userId,
      assigned_to: userId,
      status,
      type,
      payload,
      dimensions,
    });
    await truck.save();
    res.status(200).json({ message: 'Truck created successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};
// eslint-disable-next-line
const assignTruckToUser = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { userId } = req.user;
    await Truck.findOneAndUpdate(
      { assigned_to: userId },
      {
        $set: { assigned_to: null },
      },
    );
    await Truck.findOneAndUpdate({
      _id: id,
      created_by: userId,
    }, {
      $set: { assigned_to: userId },
    });
    res.status(200).json({ message: 'Truck assigned successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};
// eslint-disable-next-line
const editTruckInfo = async (req, res, next) => {
  try {
    const { type } = req.body;
    const { userId } = req.user;
    const { id } = req.params;
    await Truck.findOneAndUpdate({
      _id: id,
      created_by: userId,
    }, { type });
    res.status(200).json({ message: 'Truck details changed successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};
// eslint-disable-next-line
const deleteTruck = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { userId } = req.user;
    await Truck.findOneAndDelete({
      _id: id,
      created_by: userId,
    });
    res.status(200).json({ message: 'Truck deleted successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

module.exports = {
  getTruck,
  getTrucks,
  createTruck,
  editTruckInfo,
  assignTruckToUser,
  deleteTruck,
};
