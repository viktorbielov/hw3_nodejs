const bcrypt = require('bcryptjs');
const { User } = require('../models/User');
// eslint-disable-next-line
const getUserInfo = async (req, res, next) => {
  try {
    const { email } = req.user;
    const { _id, role, createdAt } = await User.findOne({ email });
    res.status(200).send({
      _id,
      role,
      email,
      createdDate: createdAt,
    });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};
// eslint-disable-next-line
const deleteUserAccount = async (req, res, next) => {
  try {
    const { userId } = req.user;
    await User.deleteOne({ userId });
    res.status(200).json({ message: 'Profile deleted successfully' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};
// eslint-disable-next-line
const changePassword = async (req, res, next) => {
  try {
    const { oldPassword, newPassword } = req.body;
    const { email } = req.user;
    const { password } = await User.findOne({ email });

    if (await bcrypt.compare(oldPassword, password)) {
      await User.findOneAndUpdate({ email }, {
        $set: {
          password: await bcrypt.hash(newPassword, 10),
        },
      });
      return res.status(200).json({ message: 'Password changed successfully' });
    }
    return res.status(401).json({ message: 'Old password is incorrect' });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

module.exports = {
  getUserInfo,
  deleteUserAccount,
  changePassword,
};
