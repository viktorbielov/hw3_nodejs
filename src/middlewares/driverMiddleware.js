const driverMiddleware = async (req, res, next) => {
  if (req.user.role === 'DRIVER') {
    next();
  } else {
    res.status(403).json({ message: 'You have to be a driver to proceed.' });
  }
};

module.exports = { driverMiddleware };
