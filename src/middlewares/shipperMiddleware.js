const shipperMiddleware = async (req, res, next) => {
  if (req.user.role === 'SHIPPER') {
    next();
  } else {
    res.status(403).json({ message: 'You have to be a shipper to proceed' });
  }
};

module.exports = { shipperMiddleware };
