const mongoose = require('mongoose');

const { Schema } = mongoose;

const userSchema = new Schema({
  role: {
    type: String,
    required: true,
    enum: ['SHIPPER', 'DRIVER'],
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
}, {
  timestamps: {
    createdAt: 'created_date',
    updatedAt: 'updated_date',
  },
});

const User = mongoose.model('users', userSchema);

module.exports = { User };
