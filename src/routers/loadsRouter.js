const express = require('express');
const { authMiddleware } = require('../middlewares/authMiddleware');
const { driverMiddleware } = require('../middlewares/driverMiddleware');
const { shipperMiddleware } = require('../middlewares/shipperMiddleware');
const {
  registerLoad,
  postUsersLoads,
  getUsersLoads,
  getLoad,
  getActiveLoad,
  editLoadDetails,
  toggleLoadState,
  shippingInfo,
  deleteLoad,
} = require('../controllers/loads.controller');

const router = express.Router();

router.post('/', authMiddleware, registerLoad, shipperMiddleware);
router.get('/', authMiddleware, getUsersLoads);
router.get('/active', authMiddleware, driverMiddleware, getActiveLoad);
router.post('/:id/post', authMiddleware, shipperMiddleware, postUsersLoads);
router.get('/:id', authMiddleware, getLoad);
router.patch('/active/state', authMiddleware, driverMiddleware, toggleLoadState);
router.put('/:id', authMiddleware, shipperMiddleware, editLoadDetails);
router.delete('/:id', authMiddleware, shipperMiddleware, deleteLoad);
router.get('/:id/shipping_info', authMiddleware, shipperMiddleware, shippingInfo);

module.exports = { loadsRouter: router };
