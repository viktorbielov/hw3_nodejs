const express = require('express');
const { register, login, forgotPassword } = require('../controllers/auth.controller');

const router = express.Router();

router.post('/register', register);
router.post('/login', login);
router.post('/forgot_password', forgotPassword);

module.exports = { authRouter: router };
