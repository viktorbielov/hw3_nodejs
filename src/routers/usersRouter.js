const express = require('express');
const { authMiddleware } = require('../middlewares/authMiddleware');
const {
  getUserInfo,
  deleteUserAccount,
  changePassword,
} = require('../controllers/users.controller');

const router = express.Router();

router.get('/me', authMiddleware, getUserInfo);
router.patch('/me/password', authMiddleware, changePassword);
router.delete('/me', authMiddleware, deleteUserAccount);

module.exports = {
  usersRouter: router,
};
