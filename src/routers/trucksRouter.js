const express = require('express');
const { authMiddleware } = require('../middlewares/authMiddleware');
const { driverMiddleware } = require('../middlewares/driverMiddleware');
const {
  getTruck,
  getTrucks,
  createTruck,
  editTruckInfo,
  assignTruckToUser,
  deleteTruck,
} = require('../controllers/trucks.controller');

const router = express.Router();

router.get('/', authMiddleware, driverMiddleware, getTrucks);
router.post('/', authMiddleware, driverMiddleware, createTruck);
router.get('/:id', authMiddleware, driverMiddleware, getTruck);
router.put('/:id', authMiddleware, driverMiddleware, editTruckInfo);
router.delete('/:id', authMiddleware, driverMiddleware, deleteTruck);
router.post('/:id/assign', authMiddleware, driverMiddleware, assignTruckToUser);

module.exports = {
  trucksRouter: router,
};
