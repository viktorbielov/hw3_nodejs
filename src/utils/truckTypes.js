// eslint-disable-next-line
const getDimensionsFromType = (type) => {
  // eslint-disable-next-line
  switch (type) {
    case 'SPRINTER':
      return {
        length: 300,
        width: 250,
        height: 170,
      };
    case 'SMALL STRAIGHT':
      return {
        length: 500,
        width: 250,
        height: 170,
      };
    case 'LARGE STRAIGHT':
      return {
        length: 700,
        width: 350,
        height: 200,
      };
  }
};

// eslint-disable-next-line
const getPayloadFromType = (type) => {
  // eslint-disable-next-line
  switch (type) {
    case 'SPRINTER':
      return 1700;
    case 'SMALL STRAIGHT':
      return 2500;
    case 'LARGE STRAIGHT':
      return 4000;
  }
};

module.exports = { getDimensionsFromType, getPayloadFromType };
