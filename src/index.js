require('dotenv').config();
const express = require('express');
// eslint-disable-next-line import/no-extraneous-dependencies
const morgan = require('morgan');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const app = express();

const { authRouter } = require('./routers/authRouter');
const { usersRouter } = require('./routers/usersRouter');
const { trucksRouter } = require('./routers/trucksRouter');
const { loadsRouter } = require('./routers/loadsRouter');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('tiny'));
app.use(cookieParser());

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

const start = async () => {
  try {
    const port = process.env.PORT || 3000;
    await mongoose.connect(process.env.DB_URI);
    app.listen(port, () => {
      console.log(`Server is running on port ${port}`);
    });
  } catch (err) {
    console.log(`Error on server startup: ${err}`);
  }
};

start();

/**
 * Error handler
 * @param {object} err The first number.
 * @param {object} req The second number.
 * @param {object} res sum of the two numbers.
 * @param {object} next sum of the two numbers.
 */
// eslint-disable-next-line no-unused-vars
function errorHandler(err, req, res, next) {
  if (err.status === 400 || err.status === 404) {
    res.status(err.status).send({ message: err.message });
  }
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}

// ERROR HANDLER
app.use(errorHandler);
